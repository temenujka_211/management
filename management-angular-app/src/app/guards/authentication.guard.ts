import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { AuthService } from "../components/modules/authentication/services/auth.service";


@Injectable({

  providedIn: 'root'
})

export class AuthenticationGuard implements /*CanLoad*/ CanActivate {

  constructor(

    private service : AuthService,
    private route   : Router
  ) { }

  canActivate() {

    const isAuthenticated = this.service.isUserLogged();

    if (isAuthenticated === true) {

      return true;
    } else {
      
      this.service.removeTokens();
      this.route.navigate(['auth']);
      return false;
    }
  }
}