import { Component, OnInit } from '@angular/core';
import { AuthService } from '../modules/authentication/services/auth.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  isLoggedIn = localStorage.getItem("accessToken");

  constructor(private authSevice: AuthService) { }

  ngOnInit(): void {
  }

   onLogout() : void {
    
    this.authSevice.logout();
  }
}
