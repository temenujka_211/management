import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { AuthComponent } from "./auth/auth.component";
import { AuthRoutingModule } from "./authentication-routing.module";
import { LoginComponent } from "./login/login.component";


@NgModule({
    imports:[
        CommonModule,
        HttpClientModule,
        ReactiveFormsModule,
        AuthRoutingModule
    ],
    declarations: [
        LoginComponent,
        AuthComponent
    ]
})
export class AuthenticationModule {

}