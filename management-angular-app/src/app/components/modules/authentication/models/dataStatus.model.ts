export enum State {

    unauthorized = 401,
    created = 201,
    forbidden = 403,
    notFound = 404,
    internalServerError = 500
}