export interface AccessRefreshToken {
    accessToken: string;
    refreshToken: string;
  }