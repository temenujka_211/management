import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { AuthService } from "./auth.service";



const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  const url =  `${environment.baseUrl}Auth/refresh`;
  @Injectable({
    providedIn: 'root'
  })

  export class RefreshTokenService { 

    constructor(private http: HttpClient, private authService: AuthService) { }

    refreshToken(token: string) {
        const tokens = this.http.post(url, {
            refreshToken: token,
            accessToken: this.authService.getAccessToken()
          }, httpOptions);


      return tokens; 
    }
  }
  