import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";

@Injectable({

    providedIn: 'root'
})

export class AuthService {

    private url = `${environment.baseUrl}Auth/`;

    constructor(private http: HttpClient, private router: Router) { }

    getAccessToken() {

        const accessToken = localStorage.getItem('accessToken');

        return accessToken;
    }

    getRefreshToken() {

        const refreshToken = localStorage.getItem('refreshToken');

        return refreshToken;
    }

    getTokens() {

        const refreshToken = localStorage.getItem('refreshToken');
        const accessToken = localStorage.getItem('accessToken');

        return { accessToken, refreshToken };
    }

    saveTokens(accessToken: string, refreshToken: string) {

        localStorage.setItem('accessToken', accessToken);
        localStorage.setItem('refreshToken', refreshToken);
    }

    removeTokens() {

        localStorage.clear();
    }

    isUserLogged() {

        const isLogged = this.getTokens();

        if (isLogged != null) {

            return true;
        }

        return false;
    }

    generateRefreshToken() {
        let body = {
            'accessToken': this.getAccessToken(),
            'refreshToken': this.getRefreshToken()
        };

        console.log(this.http.post(`${this.url}refresh`, body));

        return this.http.post(`${this.url}refresh`, body);
    }

    login(usercred: any) {

        const tokens = this.http.post(`${this.url}token`, usercred)

        return tokens;
    }

    logout() {

        alert('Your session has been terminated!');

        this.removeTokens();

        this.router.navigate(['/auth'])
            .then(() => {

                location.reload();
            });
    }
}