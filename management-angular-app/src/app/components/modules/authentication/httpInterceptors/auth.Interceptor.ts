import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable, Injector } from "@angular/core";
import { catchError, Observable, switchMap, throwError } from "rxjs";
import { State } from "../models/dataStatus.model";
import { AuthService } from "../services/auth.service";


@Injectable({
    providedIn: 'root'
})


export class AuthInterceptor implements HttpInterceptor {

    constructor(private inject: Injector) { }

    intercept(request: HttpRequest<any>,
              next   : HttpHandler): 
                       Observable<HttpEvent<any>> {

        let authService = this.inject.get(AuthService);
        let authRequest = request;
        let accessToken = authService.getAccessToken();

        
        authRequest = this.addBearerHeader(request, accessToken);


        return next.handle(authRequest).pipe(
                   catchError(errordata => {

                    let errorDataStatus  = errordata.status;
                    let unauthorized     = State.unauthorized;
                    
                    if (errorDataStatus === unauthorized) {
                    
                      const generateNewTokens = this.generateNewRefreshToken(request, next);

                      return generateNewTokens;
                }
                return throwError(errordata);
            })
        );

    }

    generateNewRefreshToken(request: HttpRequest<any>, 
                            next   : HttpHandler) {

        let authService = this.inject.get(AuthService);

        return authService.generateRefreshToken().pipe(

          switchMap((data: any) => {

            const accessToken  = data.accessToken;
            const refershToken = data.refreshToken;

            authService.saveTokens(accessToken, refershToken);
            
            const bearerHeader = this.addBearerHeader(request, accessToken);

            return next.handle(bearerHeader)
          }),
          catchError(errors =>{

            authService.logout();

            return throwError(errors)
          })
        );
      }
    
    addBearerHeader(request: HttpRequest<any>, 
                    token  : any) {

          const bearerHeader = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
      
          return bearerHeader;
    }

}