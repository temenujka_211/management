import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  response: any;
  formGroup!: FormGroup

  constructor(

    private formbuilder   : FormBuilder,
    private router        : Router,
    private authService   : AuthService) { }

  get usernameFormControl(): FormControl {

      return this.formGroup?.get('username') as FormControl;
    }
  
   get passwordFormControl(): FormControl {
  
      return this.formGroup?.get('password') as FormControl;
    }
  
    
  ngOnInit(): void {

    this.formGroup = this.formbuilder.group({

      username: ['', [Validators.required,
                      Validators.minLength(3),
                      Validators.maxLength(16)]],

      password: ['', [Validators.required,
                      Validators.minLength(5),
                      Validators.maxLength(255)]],

    });
  }

  onSubmit(): void {

    const isFormGroupValid = this.formGroup.valid
   
    if (!isFormGroupValid) {

      alert("Login failed username or  password is incorrect!");
      location.reload();
    }
    else {
      const loginFormInformation = this.formGroup.value;

      this.authService.login(loginFormInformation)
        .subscribe(result => {

          this.response = result;

          if (this.response != null) {

            const accessToken = this.response.accessToken;
            const refreshToken = this.response.refreshToken;

            this.authService.saveTokens(accessToken, refreshToken);

            this.router.navigate(['/'])
              .then(() => {

                location.reload();

              });
          }
        })
    }
  }
}
