import { CommonModule } from "@angular/common";
import { HttpClientModule} from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSortModule } from "@angular/material/sort";
import { MatTableModule } from "@angular/material/table";
import { RouterModule } from "@angular/router";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";
import { MainRoutingModule } from "./main-routing-module";
import { MainComponent } from "./main.component";
import { UsersComponent } from "./users/users.component";

@NgModule ({

    imports : [
        HttpClientModule,
        ReactiveFormsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        FormsModule,
        MainRoutingModule,
        RouterModule,
        CommonModule
    ],
    declarations: [
        MainComponent,
        CreateComponent,
        EditComponent,
        UsersComponent
    ]
})

export class MainModule {

}