import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { AuthService } from "../../authentication/services/auth.service";
import { User } from "../models/user.model";


@Injectable({
    providedIn: 'root'
})

export class UserService {

    private url = `${environment.baseUrl}UserManagement/`;

    constructor(private http: HttpClient,
        private authService: AuthService) { }

    private token = this.authService.getAccessToken();


    getUsers() {

        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.token}`
        }

        const users = this.http.get<User[]>(`${this.url}all`, { headers });

        return users;
    }

    getUser(id: Number): Observable<User> {

        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.token}`
        }

        const user = this.http.get<User>(`${this.url}userById/${id}`, { headers });

        return user;
    }

    postUser(user: any) {

        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.token}`
        };

        const createdUser = this.http.post(`${this.url}create`, user, { headers });

        return createdUser;
    }

    patchUser(user: User) {

        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.token}`
        }
        const editedUser = this.http.patch<User>(`${this.url}edit/${user.id}`, user, { headers });

        return editedUser;
    }

    deleteUser(id: Number) {

        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.token}`
        }
        const deletedUser = this.http.delete(`${this.url}delete/${id}`, {headers});

        return deletedUser;
    }
}