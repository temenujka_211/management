export interface User {
    [x: string]: any;
    id?: number,
    firstName: string,
    lastName: string,
    email: string,
    username: string,
    password: string
}