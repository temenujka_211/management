import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { User } from 'src/app/components/modules/main/models/user.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { UserService } from '../services/users.service';
import { RefreshTokenService } from '../../authentication/services/refreshToken.service';
import { AuthService } from '../../authentication/services/auth.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements AfterViewInit {


  users!: User[];
  displayedColumns: string[] = ['firstName', 'lastName', 'email', 'username', 'options'];
  dataSource = new MatTableDataSource<User>;

  constructor(
    private userService: UserService,
    private refreshService : RefreshTokenService,
    private authService: AuthService ) { }


  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngAfterViewInit(): void {

    this.getContent();
  }

  searchFilter(e: Event) {

    const filterValue = (e.target as HTMLInputElement).value;

    this.dataSource.filter = filterValue.
                              trim().
                              toLowerCase();

      const isSearchBoxEmpty = this.dataSource.paginator;
                              
    if (isSearchBoxEmpty) {

        isSearchBoxEmpty.firstPage();
    }
  }

   onDelete(id: Number) {

   const request$ = this.userService.deleteUser(id);

   alert(`Are you sure you want to delete user with id ${id}?`);

      request$.subscribe({
        next:() =>{

          location.reload();
        }
      });
}


  private getContent() {

    this.userService.getUsers().subscribe(res => {
      
     if(res){

      this.users                = res;
      this.dataSource           = new MatTableDataSource(this.users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort      = this.sort;
  
     }

      const token = this.authService.getRefreshToken();

      if(token){

        this.refreshService.refreshToken(token);
      }

    });
  }
}



