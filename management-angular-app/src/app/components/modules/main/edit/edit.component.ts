import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { of, Subject, switchMap, takeUntil } from 'rxjs';
import { User } from 'src/app/components/modules/main/models/user.model';
import { UserService } from '../services/users.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  formGroup !: FormGroup;
  user      !: User;
  response: any
  destroy$ = new Subject<boolean>();

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private service: UserService,) {

    this.user = {
      id: 0,
      firstName: '',
      lastName: '',
      email: '',
      username: '',
      password: ''
    };

  }

  get firstnameFormControl(): FormControl {

    return this.formGroup?.get('firstName') as FormControl;
  }

  get lastnameFormControl(): FormControl {

    return this.formGroup?.get('lastName') as FormControl;
  }

  get emailFormControl(): FormControl {

    return this.formGroup?.get('email') as FormControl;
  }

  get usernameFormControl(): FormControl {

    return this.formGroup?.get('username') as FormControl;
  }

  get passwordFormControl(): FormControl {

    return this.formGroup?.get('password') as FormControl;
  }


  ngOnInit(): void {


    this.route.params.pipe(
      switchMap((params: any) => {

        const idToEdit = params.id;

        if (idToEdit !== 0) {

          const userInformation = this.service.getUser(params.id);

          return userInformation;
        }

        this.initForm();

        return of(null);
      }),
      takeUntil(this.destroy$)
    ).subscribe({
      next: (response) => {

        if (response) {

          this.user = response['data'];
          this.initForm();
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  onSubmit() {

    const isFormGroupInvalid = this.formGroup.invalid

    if (isFormGroupInvalid) {

      this.formGroup.markAllAsTouched();
      return;
    }


    const editFormInformation: User = {

      id: this.formGroup.value.id,
      firstName: this.formGroup.value.firstName,
      lastName: this.formGroup.value.lastName,
      email: this.formGroup.value.email,
      username: this.formGroup.value.username,
      password: this.formGroup.value.password,
    };

    const request = this.service.patchUser(editFormInformation);

    request.subscribe({
      next: () => {

        this.router.navigate(['users']);
      }
    });
  }

  private initForm(): void {

    this.formGroup = this.formBuilder.group({

      id: this.user.id,

      firstName: [this.user.firstName, [Validators.required,
      Validators.maxLength(50),
      Validators.minLength(2)]],

      lastName: [this.user.lastName, [Validators.required,
      Validators.maxLength(40),
      Validators.minLength(2)]],

      email: [this.user.email, [Validators.required,
      Validators.maxLength(50),
      Validators.minLength(5),
      Validators.pattern('^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],

      username: [this.user.username, [Validators.required,
      Validators.maxLength(16),
      Validators.minLength(3)]],

      password: [this.user.password, [Validators.required,
      Validators.maxLength(255),
      Validators.minLength(5)]],

    });
  }
}
