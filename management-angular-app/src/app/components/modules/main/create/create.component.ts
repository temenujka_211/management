import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { User } from 'src/app/components/modules/main/models/user.model';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/users.service';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  formGroup !: FormGroup;
  response   : any;
  user      !: User

  constructor(

    private fb      : FormBuilder,
    private router  : Router,
    private service : UserService) {
    
    this.user = {

      firstName  : '',
      lastName   : '',
      email      : '',
      username   : '',
      password   : '',
    };

  }


  get firstnameFormControl(): FormControl {

    return this.formGroup?.get('firstName') as FormControl;
  }

  get lastnameFormControl(): FormControl {

    return this.formGroup?.get('lastName') as FormControl;
  }

  get emailFormControl(): FormControl {

    return this.formGroup?.get('email') as FormControl;
  }

  get usernameFormControl(): FormControl {

    return this.formGroup?.get('username') as FormControl;
  }

  get passwordFormControl(): FormControl {

    return this.formGroup?.get('password') as FormControl;
  }


   ngOnInit(): void {

    this.formGroup = this.fb.group({

      firstName : [this.user.firstName,[Validators.required,
                                        Validators.maxLength(50), 
                                        Validators.minLength(2)]],

      lastName  : [this.user.lastName,[Validators.required,
                                       Validators.maxLength(40),
                                       Validators.minLength(2)]],

      email     : [this.user.email, [Validators.required,
                                     Validators.maxLength(50),
                                     Validators.minLength(5), 
                                     Validators.pattern('^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],

      username  : [this.user.username, [Validators.required,
                                        Validators.maxLength(16), 
                                        Validators.minLength(3)]],

      password  : [this.user.password, [Validators.required,
                                        Validators.maxLength(255), 
                                        Validators.minLength(5)]],
   
    });
  }

  onSubmit() {

    const isFormGroupInvalid = this.formGroup.invalid 
    
    if (isFormGroupInvalid) {

      this.formGroup.markAllAsTouched();
      return;
    }

    const createFormInformation  = this.formGroup.value;
  
    
    this.service.postUser(createFormInformation)
     .subscribe(result => {

      this.response = result;

      if(this.response !== null){

        this.router.navigate(['users'])
        .then(() => {

          location.reload();
        })
      }
    })
  }
}
