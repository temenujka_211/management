import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CreateComponent } from "./create/create.component";
import { EditComponent } from "./edit/edit.component";
import { MainComponent } from "./main.component";
import { UsersComponent } from "./users/users.component";

const routes: Routes = [{
   
    path: '',
    component: MainComponent,
    
    children: [{

        path: 'all',
        component: UsersComponent
      },{

        path: 'create',
        component: CreateComponent
      },{

        path: 'edit/:id',
        component: EditComponent,
      },{

        path: '',
        pathMatch: 'full',
        redirectTo: 'all'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}