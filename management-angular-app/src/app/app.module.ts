import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavigationComponent } from './components/navigation/navigation.component';
import { FooterComponent } from './components/footer/footer.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthService } from './components/modules/authentication/services/auth.service';
import { AuthInterceptor } from './components/modules/authentication/httpInterceptors/auth.Interceptor';

@NgModule({

  imports: [
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    NavigationComponent,
    FooterComponent,
  ],
  providers: [AuthService,{
                           provide  :HTTP_INTERCEPTORS,
                           useClass :AuthInterceptor,
                           multi    :true }],

  bootstrap: [AppComponent]
})

export class AppModule { }
