import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AuthenticationGuard } from './guards/authentication.guard';


const routes: Routes = [
  {
    path        : 'auth',
    loadChildren: () => 
        import ('./components/modules/authentication/authentication.module').
        then(a => a.AuthenticationModule)
  },
  {
     path        : 'users',
     loadChildren: () =>
       import ('./components/modules/main/main.module').
       then(m => m.MainModule),
     canActivate : [AuthenticationGuard]
  
  },{
    path      : '',
    component : HomeComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
