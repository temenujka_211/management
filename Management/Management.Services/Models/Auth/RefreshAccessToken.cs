﻿namespace Management.Services.Models.Auth
{
    public class RefreshAccessToken
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
