﻿using Management.Data;
using Management.Models.Entities;
using Management.Services.Interfaces;
using Management.Services.Models.UserManagement;
using System.Collections.Generic;
using System.Linq;

namespace Management.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext context;
        public UserService(ApplicationDbContext context)
        {
            this.context = context;
        }
        
        public List<User> AllUsers()
        {
            var users = this.context.Users
               .ToList();

            return users;
        }
       
        public User GetUserById(int userId)
        {
            var user = this.context.Users
                .Where(x => x.Id == userId)
                .FirstOrDefault();

            return user;
        }
        public User CreateUser(UserModel model)
        {

            var user = new User
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Username = model.Username,
                Password = model.Password
            };

            this.context.Users.Add(user);
            this.context.SaveChanges();

            return user;
        }
        
        public User EditUser(UserModel model, int userId)
        {
            var user = GetUserById(userId);

            if (user == null)
            {
                return user;
            }

            user.Id = model.Id;
            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Email = model.Email;
            user.Username = model.Username;
            user.Password = model.Password;

            this.context.Users.Update(user);
            this.context.SaveChanges();

            return user;
        }
        
        public User DeleteUser(int userId)
        {
            var user = GetUserById(userId);

            if (user == null)
            {
                return user;
            }

            this.context.Remove(user);
            this.context.SaveChanges();

            return user;
        }
    }
}
