﻿using System.Collections.Generic;


namespace Management.Services.Interfaces
{
    public interface ITokenGeneratorService
    {
        List<string> GenerateToken(string claimValue);
    }
}
