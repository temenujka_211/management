﻿using Management.Models.Entities;
using Management.Services.Models.UserManagement;
using System.Collections.Generic;

namespace Management.Services.Interfaces
{
    public interface IUserService
    {
        List<User> AllUsers();

        User GetUserById(int userId);
        
        User CreateUser(UserModel model);
        
        User EditUser(UserModel model, int userId);

        User DeleteUser(int userId);

    }
}
