﻿using Management.Services.Models.Auth;
using System.Collections.Generic;

namespace Management.Services.Interfaces
{
   public  interface IAuthService
    {
        List<string> AccessTokenGenerator(Credential credential);
        List<string> RefreshTokenGenerator(RefreshAccessToken model);
        bool RevokeToken(RefreshAccessToken model);
    }
}
