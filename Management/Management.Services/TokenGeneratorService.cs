﻿using Management.Services.Interfaces;
using Management.Services.Models.Auth;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;


namespace Management.Services
{
   public class TokenGeneratorService : ITokenGeneratorService
    {
        public List<string> GenerateToken(string claimValue)
        {
            var tokenModel = new RefreshAccessToken();

            var keyEncoding = Encoding.UTF8.GetBytes("Random-Private-Key");

            var key = new SymmetricSecurityKey(keyEncoding);

            var claims = new Claim[]
            {
                new Claim("LoggedUserId", claimValue)
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = new JwtSecurityToken
                (
                 // issure
                 "dev.company",
                 // audience
                 "management.react.webapi.project",
               // claims
               claims,
               // expires
               expires: DateTime.Now.AddMinutes(1),
               // signingCredentails
               signingCredentials: new SigningCredentials
                (key, SecurityAlgorithms.HmacSha256Signature)
           );

            tokenModel.AccessToken = tokenHandler.WriteToken(token);
            tokenModel.RefreshToken = Guid.NewGuid().ToString();

            var accessToken = tokenModel.AccessToken;
            var refreshToken = tokenModel.RefreshToken;

            var tokens = new List<string>
            {
                accessToken,
                refreshToken
            };

            return tokens;
        }
    }
}
