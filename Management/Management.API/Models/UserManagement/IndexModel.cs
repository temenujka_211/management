﻿using Management.API.Models.Filter;
using Management.API.Models.Pager;
using Management.Services.Models.UserManagement;
using System.Collections.Generic;

namespace Management.API.Models.UserManagement
{
    public class IndexModel
    {
        public string OrderBy { get; set; }
        public string OrderDir { get; set; }
        public virtual ICollection<UserModel> Items { get; set; }
        public FilterModel Filter { get; set; }
        public PagerModel Pager { get; set; }
    }
}
