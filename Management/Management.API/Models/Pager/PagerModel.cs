﻿namespace Management.API.Models.Pager
{
    public class PagerModel
    {
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
        public int PagesCount { get; set; }
    }
}
