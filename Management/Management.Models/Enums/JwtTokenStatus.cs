﻿namespace Management.Models.Enums
{
    public enum JwtTokenStatus
    {
       Active = 0,
       Revoked = 1
    }
}
