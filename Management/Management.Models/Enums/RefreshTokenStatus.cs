﻿namespace Management.Models.Enums
{
    public enum RefreshTokenStatus
    {
        Used = 0,
        Pending = 1,
        Revoked = 2
    }
}
