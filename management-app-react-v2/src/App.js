import './App.css';
import Home from './components/home/Home';
import Login from './components/login/Login';
import Navigation from './components/navigation/Navigation';
import Footer from './components/footer/Footer';
import {Routes, Route} from 'react-router-dom';
import Users from './components/users/Users';


function App() {
  return (
    <div className="App">
      <Navigation />
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/home" element={<Home />}></Route>
        <Route path="/users" element={<Users />}></Route>
        <Route path="/login" element={<Login />}></Route>
      </Routes>
      <Footer/>
    </div>
  );
}

export default App;
