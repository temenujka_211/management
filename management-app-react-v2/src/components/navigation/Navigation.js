import { Navigate } from 'react-router-dom';
import axiosApiInstance from '../../interceptors/axios';

async function logout() {

  await axiosApiInstance.post('Auth/revoke',
    {
      accessToken: localStorage.getItem('accessToken'),
      refreshToken: localStorage.getItem('refreshToken'),
    });
  localStorage.clear();
  <Navigate to="/" />
}
//const accessToken = localStorage.i;

function Navigation() {
  return (
    <header className="navbar navbar-expand-lg navbar-light" id="navigation">
      <div className="container-fluid">
        <a href="#" className="navbar-brand">Management</a>
        <button type="button" className="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse9">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarCollapse9">
          <div className="navbar-nav">
            <a href="/" className="nav-item nav-link active">Home</a>
            <a href="/users" className="nav-item nav-link">Users</a>
          </div>
          <form className="d-flex ms-auto">
            <a href="/login" className="btn btn-outline-primary" id="login-btn">Login</a>
            <button type="button" className="btn btn-outline-primary" onClick={logout}>Logout</button>
          </form>
        </div>
      </div>
    </header >
  )
}

export default Navigation