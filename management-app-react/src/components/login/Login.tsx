import React, { useState } from 'react'
import { Navigate } from 'react-router';
import axiosApiInstance from "../../interceptors/axios";

//import { Navigate } from 'react-router-dom';

function Login() {

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [navigate, setNavigate] = useState(false);

  const submit = async (e: { preventDefault: () => void; }) => {
    e.preventDefault();

    const { data } = await axiosApiInstance.post('Auth/token',
      {
        username,
        password
      });

    localStorage.setItem('accessToken', data.accessToken);
    localStorage.setItem('refreshToken', data.refreshToken);

    setNavigate(true);
  }

  if (navigate) {
    return  <Navigate to="/" />

  }

  return (
    <section className="text-center text-lg-start">
      <div className="container py-4" id="section">
        <div className="row g-0 align-items-center">
          <div className="col-lg-6 mb-5 mb-lg-0">
            <div className="card cascading-right" id="card">
              <div className="card-body p-5 shadow-5 text-center">
                <h2 className="fw-bold mb-5">Sign in now</h2>
                <form onSubmit={submit}>

                  <div className="form-outline mb-4" >
                    <label className="form-label" >Username</label>
                    <input type="text" id="form3Example3" className="form-control" placeholder="Username" required
                      onChange={e => setUsername(e.target.value)} />
                  </div>

                  <div className="form-outline mb-4">
                    <label className="form-label" >Password</label>
                    <input type="password" id="form3Example4" className="form-control" placeholder="Password" required
                      onChange={e => setPassword(e.target.value)} />
                  </div>
                  <button type="submit" className="btn btn-primary btn-block mb-4" >
                    Sign in
                  </button>
                </form>

              </div>
            </div>
          </div>

          <div className="col-lg-6 mb-5 mb-lg-0">
            <img id="login" src="https://mdbootstrap.com/img/new/ecommerce/vertical/004.jpg"
              className="w-100 rounded-4 shadow-4" alt="" />
          </div>
        </div>
      </div>
    </section>
  )
}

export default Login
